Minecraft 1.16 mod for the [Fabric modloader](https://www.fabricmc.net/). 

Download the mod on [CurseForge](https://www.curseforge.com/minecraft/mc-mods/camp-fires-cook-mobs)!

This mod makes it so that when a mob that drops meat dies from standing on a camp fire (or soul camp fire), they drop the cooked version of their meat, similarly to if they had died in lava or fire. 

It bothered me that the primary purpose of a camp fire was to cook food, and yet they didn't cook mobs. They even visually had an open flame just like normal fire, which cooks mobs. No more! 

This mod also has four configs, two for each of of the campfire types. These are the defaults:

    "standardCampFiresBurnItem": true,
    "boolean soulCampFiresBurnItems": false,
    "boolean frostWalkerProtectsFromStandardCampFires": true,
    "boolean frostWalkerProtectsFromSoulCampFires": false,

The configs can be changed either through Mod Menu if you have it installed or by editing .minecraft/config/camp_fires_cook_mobs.json

Here's a before/after pic

![Before/after](https://shft.cl/img/l/lh5.googleusercontent.com-3833414139700383.webp)

If you'd like to translate Camp Fires Cook Mobs (there are only a few lines of text), follow [this link](https://crowdin.com/project/camp-fire-cook-mobs/invite). New translations will be added once approved without the mod needing an update thanks to [CrowdinTranslate](https://github.com/gbl/CrowdinTranslate).

This mod is only for Fabric and I won't be porting it to Forge. The license is [MIT](https://will-lucic.mit-license.org/), however, so anyone else is free to port it.

I'd appreciate links back to this page if you port or otherwise modify this project, but links aren't required.
