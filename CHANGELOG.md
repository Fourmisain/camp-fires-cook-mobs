- 1.1.7 (07 Feb. 2021): Crowdin can be disabled through the config file 
- 1.1.6 (15 Jan. 2021): Translations are now handled through [CrowdinTranslate](https://crowdin.com/project/camp-fire-cook-mobs).
  Marked as compatible with 1.16.5. 
- 1.1.5 (3 Nov. 2020): Marked as compatible with 1.16.4. 
- 1.1.4 (18 Sep. 2020): Marked as compatible with 1.16.3. 