package net.sssubtlety.camp_fires_cook_mobs;

import me.sargunvohra.mcmods.autoconfig1u.AutoConfig;
import net.fabricmc.api.ModInitializer;
import net.sssubtlety.camp_fires_cook_mobs.CampFiresCookMobsConfig;
import net.sssubtlety.camp_fires_cook_mobs.mixin_helpers.CampFireEntityCheckTests;
import net.sssubtlety.camp_fires_cook_mobs.mixin_helpers.MixinUtils;

public class CampFiresCookMobsInit implements ModInitializer {
    public static final String MOD_ID = "camp_fires_cook_mobs";
    public static final CampFiresCookMobsConfig CONFIG = AutoConfig.getConfigHolder(CampFiresCookMobsConfig.class).getConfig();

    @Override
    public void onInitialize() {
        if (CONFIG.doStandardCampFiresBurnItems()) {
            if (CONFIG.doSoulCampFiresBurnItems()) {
                //both burn items
                CampFireEntityCheckTests.setIsItemBurningBlockState((blockState -> true));
            } else {
                //only standard burns items
                CampFireEntityCheckTests.setIsItemBurningBlockState(MixinUtils::stateIsStandardCampfire);
            }
        } else {
            if (CONFIG.doSoulCampFiresBurnItems()) {
                //only soul burns items
                CampFireEntityCheckTests.setIsItemBurningBlockState(MixinUtils::stateIsSoulCampfire);
            } else {
                //neither burns items
                CampFireEntityCheckTests.setIsItemBurningBlockState((blockState -> false));
            }
        }

        if (CONFIG.doesFrostWalkerProtectFromStandardCampFires()) {
            if (CONFIG.doesFrostWalkerProtectFromSoulCampFires()) {
                //protects from both
                CampFireEntityCheckTests.setIsFWIgnorantBlockState(blockState -> false);
            } else {
                //only protects from standard
                CampFireEntityCheckTests.setIsFWIgnorantBlockState(MixinUtils::stateIsSoulCampfire);
            }
        } else {
            if (CONFIG.doesFrostWalkerProtectFromSoulCampFires()) {
                //only protects from soul
                CampFireEntityCheckTests.setIsFWIgnorantBlockState(MixinUtils::stateIsStandardCampfire);
            } else {
                //protects from neither
                CampFireEntityCheckTests.setIsFWIgnorantBlockState((blockState -> true));
            }
        }
    }
}
