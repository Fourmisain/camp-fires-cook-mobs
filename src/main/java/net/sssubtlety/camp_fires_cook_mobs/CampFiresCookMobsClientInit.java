package net.sssubtlety.camp_fires_cook_mobs;

import de.guntram.mcmod.crowdintranslate.CrowdinTranslate;
import net.fabricmc.api.ClientModInitializer;

import static net.sssubtlety.camp_fires_cook_mobs.CampFiresCookMobsInit.CONFIG;
import static net.sssubtlety.camp_fires_cook_mobs.CampFiresCookMobsInit.MOD_ID;

public class CampFiresCookMobsClientInit implements ClientModInitializer {
    @Override
    public void onInitializeClient() {
        if (CONFIG.enableCrowdin) {
            CrowdinTranslate.downloadTranslations("camp-fires-cook-mobs", MOD_ID);
        }
    }
}
