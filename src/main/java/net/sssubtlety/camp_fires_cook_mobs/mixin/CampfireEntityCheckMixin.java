package net.sssubtlety.camp_fires_cook_mobs.mixin;

import net.minecraft.block.BlockState;
import net.minecraft.block.CampfireBlock;
import net.minecraft.enchantment.EnchantmentHelper;
import net.minecraft.entity.Entity;
import net.minecraft.entity.LivingEntity;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.sssubtlety.camp_fires_cook_mobs.mixin_helpers.CampFireEntityCheckTests;
import net.sssubtlety.camp_fires_cook_mobs.mixin_helpers.MixinUtils;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.Constant;
import org.spongepowered.asm.mixin.injection.ModifyConstant;

@Mixin(value = CampfireBlock.class, priority = 500)
public class CampfireEntityCheckMixin {
	@ModifyConstant(method = "onEntityCollision", constant = @Constant(classValue = LivingEntity.class, ordinal = 0))
	private Class<?> burnAllEntities(Object obj, Class<?> clazz, BlockState state, World world, BlockPos pos, Entity entity) {
		if (entity instanceof LivingEntity) {
			if (!CampFireEntityCheckTests.getIsFWIgnorantBlockState().test(state) && !EnchantmentHelper.hasFrostWalker((LivingEntity)entity)) {
				MixinUtils.burnEntity((Entity) obj);
			}
		} else if (CampFireEntityCheckTests.getIsItemBurningBlockState().test(state)) {
			MixinUtils.burnEntity((Entity) obj);
		}
		return LivingEntity.class;
	}
}
