package net.sssubtlety.camp_fires_cook_mobs.mixin.ignore_frostwalker;

import net.minecraft.block.CampfireBlock;
import net.minecraft.entity.LivingEntity;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Redirect;

@Mixin(value = CampfireBlock.class, priority = 500)
public abstract class AllCampFiresIgnoreFrostwalkerMixin {
	@Redirect(method = "onEntityCollision", at = @At(value = "INVOKE", target = "Lnet/minecraft/enchantment/EnchantmentHelper;hasFrostWalker(Lnet/minecraft/entity/LivingEntity;)Z"))
	private boolean ignoreFrostwalker(LivingEntity entity) {
		return false;
	}
}
