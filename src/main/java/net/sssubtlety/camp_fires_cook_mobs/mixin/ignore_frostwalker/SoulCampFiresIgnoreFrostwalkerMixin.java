package net.sssubtlety.camp_fires_cook_mobs.mixin.ignore_frostwalker;

import net.minecraft.block.BlockState;
import net.minecraft.block.Blocks;
import net.minecraft.block.CampfireBlock;
import net.minecraft.enchantment.EnchantmentHelper;
import net.minecraft.entity.Entity;
import net.minecraft.entity.LivingEntity;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.sssubtlety.camp_fires_cook_mobs.mixin_helpers.MixinUtils;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Redirect;

@Mixin(value = CampfireBlock.class, priority = 500)
public abstract class SoulCampFiresIgnoreFrostwalkerMixin {
	@Redirect(method = "onEntityCollision", at = @At(value = "INVOKE", target = "Lnet/minecraft/enchantment/EnchantmentHelper;hasFrostWalker(Lnet/minecraft/entity/LivingEntity;)Z"))
	private boolean hasFrostwalkerAndRespect(LivingEntity livingEntity, BlockState state, World world, BlockPos pos, Entity entity) {
		//false (ignore frostwalker if soul campfire
		return !MixinUtils.doesStateMatchBlock(state, Blocks.SOUL_CAMPFIRE) &&
				//respect frostwalker if standard campfire
				EnchantmentHelper.hasFrostWalker(livingEntity);
	}
}
