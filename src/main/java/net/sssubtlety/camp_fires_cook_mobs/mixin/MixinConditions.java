package net.sssubtlety.camp_fires_cook_mobs.mixin;

import me.sargunvohra.mcmods.autoconfig1u.AutoConfig;
import me.sargunvohra.mcmods.autoconfig1u.serializer.GsonConfigSerializer;
import net.sssubtlety.camp_fires_cook_mobs.CampFiresCookMobsConfig;
import net.sssubtlety.camp_fires_cook_mobs.mixin_helpers.CampFireEntityCheckTests;
import net.sssubtlety.camp_fires_cook_mobs.mixin_helpers.MixinUtils;
import org.objectweb.asm.tree.ClassNode;
import org.spongepowered.asm.mixin.extensibility.IMixinConfigPlugin;
import org.spongepowered.asm.mixin.extensibility.IMixinInfo;

import java.util.List;
import java.util.Set;

public class MixinConditions implements IMixinConfigPlugin {
    static {
        AutoConfig.register(CampFiresCookMobsConfig.class, GsonConfigSerializer::new);
    }

    private static final CampFiresCookMobsConfig CONFIG = AutoConfig.getConfigHolder(CampFiresCookMobsConfig.class).getConfig();

    @Override
    public void onLoad(String mixinPackage) {

    }

    @Override
    public String getRefMapperConfig() { return null; }

    @Override
    public boolean shouldApplyMixin(String targetClassName, String mixinClassName) {
//        if (mixinClassName.contains("CampFireBlockIgnoresFrostwalkerMixin")) {
//            return !CONFIG.doesFrostWalkerProtectFromStandardCampFires();
//        } else if (CONFIG.doStandardCampFiresBurnItems()) {
//            if (CONFIG.doesFrostWalkerProtectFromStandardCampFires()) {
//                return mixinClassName.contains("CampFireBlockSetsAllEntitiesOnFireRespectsFrostwalkerMixin");
//            } else {
//                return mixinClassName.contains("CampFireBlockSetsAllEntitiesOnFireIgnoresFrostwalkerMixin");
//            }
//        } else {
//            return mixinClassName.contains("CampFireBlockSetsLivingEntitiesOnFireMixin");
//        }
        if (mixinClassName.contains("AllCampfiresSetLivingEntitiesOnFireMixin")) {
            return true;
        } else if (mixinClassName.contains("CampfireEntityCheckMixin")) {
            return  CONFIG.doStandardCampFiresBurnItems() ||
                    CONFIG.doSoulCampFiresBurnItems() ||
                    !CONFIG.doesFrostWalkerProtectFromStandardCampFires() ||
                    !CONFIG.doesFrostWalkerProtectFromSoulCampFires();
        } else if (CONFIG.doesFrostWalkerProtectFromStandardCampFires()) {
            if (CONFIG.doesFrostWalkerProtectFromSoulCampFires()) {
                //protects from both, leave vanilla behavior
                return false;
            } else {
                //only protects from standard, soul ignores
                return mixinClassName.contains("SoulCampFiresIgnoreFrostwalkerMixin");
            }
        } else {
            if (CONFIG.doesFrostWalkerProtectFromSoulCampFires()) {
                //only protects from soul, standard ignores
                return mixinClassName.contains("StandardCampFiresIgnoreFrostwalkerMixin");
            } else {
                //protects from neither
                return mixinClassName.contains("AllCampFiresIgnoreFrostwalkerMixin");
            }
        }

    }

    @Override
    public void acceptTargets(Set<String> myTargets, Set<String> otherTargets) { }

    @Override
    public List<String> getMixins() { return null; }

    @Override
    public void preApply(String targetClassName, ClassNode targetClass, String mixinClassName, IMixinInfo mixinInfo) { }

    @Override
    public void postApply(String targetClassName, ClassNode targetClass, String mixinClassName, IMixinInfo mixinInfo) { }
}
