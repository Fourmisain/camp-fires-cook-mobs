package net.sssubtlety.camp_fires_cook_mobs.mixin;

import net.minecraft.block.BlockState;
import net.minecraft.block.CampfireBlock;
import net.minecraft.enchantment.EnchantmentHelper;
import net.minecraft.entity.Entity;
import net.minecraft.entity.LivingEntity;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.sssubtlety.camp_fires_cook_mobs.mixin_helpers.CampFireEntityCheckTests;
import net.sssubtlety.camp_fires_cook_mobs.mixin_helpers.MixinUtils;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

@Mixin(value = CampfireBlock.class, priority = 500)
public abstract class AllCampfiresSetLivingEntitiesOnFireMixin {
	@Inject(method = "onEntityCollision", at = @At(value = "INVOKE", target = "Lnet/minecraft/entity/Entity;damage(Lnet/minecraft/entity/damage/DamageSource;F)Z"))
	private void burnLivingEntities(BlockState state, World world, BlockPos pos, Entity entity, CallbackInfo ci) {
//		entity.setFireTicks(entity.getFireTicks() + 1);
//		if (entity.getFireTicks() == 0) {
//			entity.setOnFireFor(8);
//		}
		if (CampFireEntityCheckTests.getIsFWIgnorantBlockState().test(state) || entity instanceof LivingEntity && !EnchantmentHelper.hasFrostWalker((LivingEntity)entity)) {
			MixinUtils.burnEntity(entity);
		}
	}
}
