package net.sssubtlety.camp_fires_cook_mobs;

import me.sargunvohra.mcmods.autoconfig1u.ConfigData;
import me.sargunvohra.mcmods.autoconfig1u.annotation.Config;

@Config(name = "camp_fires_cook_mobs")
public class CampFiresCookMobsConfig implements ConfigData {
    boolean standardCampFiresBurnItems = true;
    boolean soulCampFiresBurnItems = false;
    boolean frostWalkerProtectsFromStandardCampFires = true;
    boolean frostWalkerProtectsFromSoulCampFires = false;
    boolean enableCrowdin = true;

    public boolean doStandardCampFiresBurnItems() {
        return standardCampFiresBurnItems;
    }

    public boolean doSoulCampFiresBurnItems() {
        return soulCampFiresBurnItems;
    }

    public boolean doesFrostWalkerProtectFromStandardCampFires() {
        return frostWalkerProtectsFromStandardCampFires;
    }

    public boolean doesFrostWalkerProtectFromSoulCampFires() {
        return frostWalkerProtectsFromSoulCampFires;
    }

}
