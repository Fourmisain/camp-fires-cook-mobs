package net.sssubtlety.camp_fires_cook_mobs.mixin_helpers;

import net.minecraft.block.BlockState;
import net.minecraft.entity.Entity;

import java.util.function.Predicate;

public class CampFireEntityCheckTests {
    static Predicate<BlockState> isFWIgnorantBlockState;
    static Predicate<BlockState> isItemBurningBlockState;

    public static Predicate<BlockState> getIsFWIgnorantBlockState() {
        return isFWIgnorantBlockState;
    }

    public static void setIsFWIgnorantBlockState(Predicate<BlockState> isFWIgnorantBlockState) {
        if (CampFireEntityCheckTests.isFWIgnorantBlockState != null) {
            throw new IllegalStateException("camp_fires_cook_mobs: CampFireEntityCheckTests's setIgnorantBlockState called more than once");
        }
        CampFireEntityCheckTests.isFWIgnorantBlockState = isFWIgnorantBlockState;
    }

    public static Predicate<BlockState> getIsItemBurningBlockState() {
        return isItemBurningBlockState;
    }

    public static void setIsItemBurningBlockState(Predicate<BlockState> isItemBurningBlockState) {
        if (CampFireEntityCheckTests.isItemBurningBlockState != null) {
            throw new IllegalStateException("camp_fires_cook_mobs: CampFireEntityCheckTests's setIsItemBurningBlockState called more than once");
        }
        CampFireEntityCheckTests.isItemBurningBlockState = isItemBurningBlockState;
    }
}
