package net.sssubtlety.camp_fires_cook_mobs.mixin_helpers;

import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.block.Blocks;
import net.minecraft.block.CampfireBlock;
import net.minecraft.entity.Entity;
import net.minecraft.entity.LivingEntity;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.Constant;
import org.spongepowered.asm.mixin.injection.ModifyConstant;

public interface CampFiresSetAllEntitiesOnFire {
	default Class<?> burnAllEntities(Object obj, BlockState state, Entity entity) {
		if (entityCheck(obj, state, entity)) {
			MixinUtils.burnEntity((Entity) obj);
		}
		return LivingEntity.class;
	}

	boolean entityCheck(Object obj, BlockState state, Entity entity);
}
