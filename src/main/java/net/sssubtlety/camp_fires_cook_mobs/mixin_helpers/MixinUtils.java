package net.sssubtlety.camp_fires_cook_mobs.mixin_helpers;


import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.block.Blocks;
import net.minecraft.entity.Entity;
import net.minecraft.util.registry.Registry;

public interface MixinUtils {

    static void burnEntity(Entity entity) {
        entity.setFireTicks(entity.getFireTicks() + 1);
        if (entity.getFireTicks() == 0) {
            entity.setOnFireFor(8);
        }
    }

    static boolean doesStateMatchBlock(BlockState state, Block block) {
        return Registry.BLOCK.getId(state.getBlock()) == Registry.BLOCK.getId(block);
    }

    static boolean stateIsStandardCampfire(BlockState state) {
        return doesStateMatchBlock(state, Blocks.CAMPFIRE);
    }

    static boolean stateIsSoulCampfire(BlockState state) {
        return doesStateMatchBlock(state, Blocks.SOUL_CAMPFIRE);
    }
}
